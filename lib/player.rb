class HumanPlayer
  def initialize(name)
    @name = name
  end

  def get_play
    puts("Place your move!")
    user_input = gets.chomp
    parsed_input = user_input.delete(" ").split(",")
    [parsed_input[0].to_i, parsed_input[1].to_i]
  end
end

class ComputerPlayer
  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  require 'byebug'

  def get_play
    @board.computer_hittable_positions.sample
  end
end
