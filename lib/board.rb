class Board
  attr_reader :grid

  def initialize(grid = nil)
    @grid = grid || Board.default_grid
  end

  def self.default_grid
    @grid = Array.new(10) { Array.new(10) }
  end

  def count
    count = 0
    @grid.each do |row|
      row.each { |el| count += 1 if el == :s }
    end
    count
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, entry)
    x, y = pos
    @grid[x][y] = entry
  end

  def empty?(pos = nil)
    return self[pos].nil? unless pos.nil?
    self.count > 0 ? false : true
  end

  def full?
    self.count == grid.length**2 ? true : false
  end

  def place_random_ship
    raise if full?
    new_ship_location = empty_positions.sample
    self[new_ship_location] = :s
  end

  def won?
    count == 0 ? true : false
  end

  def display
    grid.each do |row|
      row.each { |el| el.nil? ? print(".") : print(el) }
      puts
    end
  end

  def populate_grid
    grid.length.times { place_random_ship }
  end

  def in_range?
  end

  def empty_positions
    position_list = positions
    position_list.select { |el| self.empty?(el) }
  end

  def computer_hittable_positions
    position_list = positions
    position_list.select { |el| self.empty?(el) || self[el] == :s }
  end

  private

  def positions
    position_list = []

    @grid.length.times do |row|
      @grid.length.times { |col| position_list << [row, col] }
    end

    position_list
  end
end
