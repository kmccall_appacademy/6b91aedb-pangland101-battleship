class BattleshipGame
  require_relative 'player'
  require_relative 'board'
  attr_reader :board, :player

  def initialize(player, board = Board.new)
    @board = board
    @player = player
  end

  def attack(pos)
    x, y = pos
    if board.grid[x][y] == :s
      board.grid[x][y] = :h
    else
      board.grid[x][y] = :x
    end
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    attack(player.get_play)
  end

  def play
    board.populate_grid

    player.display(board) if player.instance_of?(ComputerPlayer)

    until game_over?
      display_status
      play_turn
    end

    puts("Game is over.")
  end

  def display_status
    board.display
    puts "There are still ships left! Keep at it!"
  end
end
